<?php
/**
 * Created by PhpStorm.
 * User: Abdugani Adikhanov
 * Email: y2k.gostop@gmail.com
 * Date: 22/11/18
 * Time: 11:45 PM
 */
namespace core\cost;


interface CalculatorInterface
{
    public function getCost(array $items);
}