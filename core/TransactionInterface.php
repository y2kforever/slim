<?php
/**
 * Created by PhpStorm.
 * User: Abdugani Adikhanov
 * Email: y2k.gostop@gmail.com
 * Date: 3/12/18
 * Time: 3:36 PM
 */

namespace core;

interface TransactionInterface
{
    public function beginTransaction();
    public function commit();
    public function rollBack();
}